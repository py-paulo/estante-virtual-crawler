from typing import List

import scrapy

from scrapy import Selector

from .utils import _text_processing_from_html, _get_year, _get_weight


class BooksSpider(scrapy.Spider):
    name = "books"
    allowed_domains = ['www.estantevirtual.com.br']
    start_urls = [
        "https://www.estantevirtual.com.br/garimpepor/sebos-e-livreiros/PE?pagina=1"
    ]

    def parse(self, response):
        bookstores: List[Selector] = response.css('a.card__livreiro')
        for bookstore in bookstores:
            # yield {
            #     'url': bookstore.attrib['href'],
            #     'title': bookstore.css('h2.card__livreiro__titulo::text').get(),
            #     'locale': bookstore.css('span.card__livreiro__cidade::text').get(),
            #     'amount': bookstore.css('span.acervo__quantidade::text').get()
            # }
            yield response.follow(bookstore.attrib['href'], callback=self.parse_list_books)

        try:
            response.css('button.ir--proxima').attrib['disabled']
            next_page = False
        except KeyError:
            next_page = True
        # except IndexError:
        #     next_page = False

        if next_page:
            yield response.follow(
                response.url.split("=")[0] +\
                    '=' +\
                    str(
                        int(response.url.split("=")[-1]
                        ) + 1
                    ),
                callback=self.parse
                )

    def parse_list_books(self, response):
        books: List[Selector] = response.css('div.exemplar')

        for book in books:
            url_book = book.css('div.wrapper__exemplar-info a::attr(href)').get()
            yield response.follow(url_book, callback=self.parse_book)

        next_page = response.css('a.next')
        if not 'desativado' in next_page.attrib['class']:
            yield response.follow(next_page.attrib['href'], callback=self.parse)

    def parse_book(self, response: scrapy.http.response.html.HtmlResponse):
        info_type = _text_processing_from_html(response.css('p.info-type::text').get())
        info_year = _get_year(str(response.css('p.info-year').get()))
        info_shelf = response.css('p.info-shelf').css('a::text').get()
        info_weight = _get_weight(str(response.css('p.info-weight').get()))
        if response.css('p.info-isbn::text').getall():
            info_isbn = _text_processing_from_html(
                response.css('p.info-isbn::text').getall()[-1])
        else:
            info_isbn = None
        info_language = _text_processing_from_html(
            response.css('p.info-language::text').getall()[-1])
        info_time = _text_processing_from_html(
            response.css('p.info-time::text').getall()[-1])
        info_description = _text_processing_from_html(
            response.css('p.livro-specs').css('span.description-text::text').get())
        url_vendor = response.css('p.seller-subtitle a::attr(href)').get()
        vendor = response.css('p.seller-subtitle a span::text').get()
        author = response.css('h2.livro-autor a span::text').get()

        page = response.url

        yield {
            'title': response.css('h1.livro-titulo::text').get().replace('\n', '').strip(),
            'author': author,
            'vendor': vendor,
            'type': info_type,
            'year': info_year,
            'shelf': info_shelf,
            'weight': info_weight,
            'isbn': info_isbn,
            'language': info_language,
            'time': info_time,
            'url': page,
            'url_vendor': url_vendor,
            'img': response.css('img.livro-capa::attr(src)').get(),
            'specs': info_description,
        }
