import json
import pprint

from typing import List

import click

pp = pprint.PrettyPrinter(indent=4, compact=True)


@click.command()
@click.option('--json-file', default='getbooks/books.json', help='Arquivo JSON com informações dos livros.')
@click.option('--key', help='Chave para procura a busca.')
@click.option('--value', help='Valor para fazer a busca.')
@click.option(
    '--contains/--no-contains', default=True,
    help="Se o 'value' deve conter o valor, caso marcado como False a busca será equivalente '=='.")
def cli_books(json_file, key, value, contains):
    """
    CLI para fazer busca no arquivo JSON gerador pelo crawler.
    """
    books: List[dict] = json.load(open(json_file, 'r', encoding='utf-8', errors='ignore'))
    for book in books:
        if value.lower() in book.get(key).lower():
            pp.pprint(book)


if __name__ == '__main__':
    cli_books()
