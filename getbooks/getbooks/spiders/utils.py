import re


def _get_weight(text: str):
    if text is None:
        return None
    weight = re.findall(r'\d{2,4}\w{0,2}', text)
    if weight:
        return weight[0]
    return None


def _get_year(text: str):
    if text is None:
        return text
    year = re.findall(r'\d\d\d\d', text)
    if year:
        return year[0]
    return None


def _text_processing_from_html(text: str):
    if text is None:
        return text
    return text.replace('\n', '').strip()
