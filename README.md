# Estante Virtual Crawler

Antes de mas nada, para quem não o conhece, a [Estante Virtual](https://www.estantevirtual.com.br/) é um portal brasileiro de comércio eletrônico que reúne o maior acervo de sebos e livreiros do Brasil. O site oferece aos leitores acesso a mais de 19,3 milhões de livros novos, seminovos e usados (números de agosto de 2019), além de raros e esgotados a preços em média mais baratos que as livrarias convencionais, [wikipédia](https://pt.wikipedia.org/wiki/Estante_Virtual).

E para ajudar os leitores a encontrar o livro que procura pelo melhor preço, ou em uma livraria/sebo mais proxima que essa ferramenta foi criada. Ela faz o *crawler* de um ou vários sebos ou livrarias e entrega um *json* com as informações de todos os livros, incluindo o link para a compra, e a partir desse arquivo você pode fazer uma busca de forma menos trabalhosa.
